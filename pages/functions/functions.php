<?php
	session_start();
	
	$tem_erro_validacao = false;
	$erros = array();
	

	function temParametroPost($parametro){
		return isset($_POST[$parametro]) && getParametroPost($parametro) != '';
	}
	
	function getParametroPost($parametro){
		return $_POST[$parametro];
	}
	function temParametroGet($parametro){
		return isset($_GET[$parametro]) && getParametroGet($parametro) != '';
	}
	
	function getParametroGet($parametro){
		return $_GET[$parametro];
	}
	
	function adicionaErro($pagina,$erro){
		header("Location: {$pagina}?error=validacao&message={$erro}");
		die();
	}
	function redirecionaPara($pagina){
		header("Location: {$pagina}");
		die();
	}
	
	function soAcessaLogadoComoAdministrador(){
		$usuario = $_SESSION["user"];
		
		if(!isset($usuario)){
			adicionaErro("/viajar/Admin","Por favor efetue seu login.");
		}
	}
	
	function sair(){
		unset($_SESSION["user"]);
	}
	
	function retornaObjetoDoFormulario($parametros){
		$retorno = array();
		foreach ($parametros as $key => $valor){
			if($valor == true){
				if(!temParametroPost($key)){
					adicionaErro("/viajar/Admin/pag_cadastro.php", "Todos os campos sao obrigatorios");
				}
			}else{
					$retorno[$key] = getParametroPost($key);
				}
		}
		
		return $retorno;
	}
	
	function retornaObjetoDoFormulario2($parametros){
		$retorno = array();
		foreach ($parametros as $key => $valor){
			if($valor == true){
				if(!temParametroPost($key)){
					adicionaErro("/viajar/Admin/promocoes.php", "Todos os campos sao obrigatorios");
				}
			}else{
					$retorno[$key] = getParametroPost($key);
				}
		}
		
		return $retorno;
	}
	
	
?>