<?php
	include '../functions/functions.php';
	include '../dao/conexao.php';
	include '../dao/cliente.dao.php';

	
	if(!temParametroPost("login")){
		adicionaErro("../../Admin", "Campo login precisa ser preenchido");
	}
	
	if(!temParametroPost("senha")){
		adicionaErro("../../Admin", "Campo senha precisa ser preenchido");
	}
	$login = getParametroPost("login");
	
	$usuario = buscar_usuario($conexao, $login);

	echo($usuario['email']);
	
	if(!isset($usuario)){
		adicionaErro("../../Admin", "Email invalido, usuario inexistente!");
	}
	
	if($usuario['senha'] != getParametroPost("senha")){
		adicionaErro("../../Admin", "Senha invalida!");
	}
	
	if($usuario['perfil'] != 'ADMINISTRADOR'){
		adicionaErro("../../Admin", "Voce so pode acessar ao painel se tiver perfil de administrador!");
	}

	$_SESSION["user"] = $usuario;
	redirecionaPara("../../Admin/pag_cadastro_pesq.php?resultado=login")
?>