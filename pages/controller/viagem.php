<?php
	include '../functions/functions.php';
	include '../dao/conexao.php';
	include '../dao/viagem.dao.php';

	$campos = array("nome" => true,  "descricao" => true, "valor" => true, "datalimite" => true, "categoria" => true,"localizacao" => true);
	
	$viagem = retornaObjetoDoFormulario($campos);
	salvar_viagem($conexao, $viagem);
	
	redirecionaPara("../../Admin/pag_cadastro_pesq.php?resultado=sucesso")
?>