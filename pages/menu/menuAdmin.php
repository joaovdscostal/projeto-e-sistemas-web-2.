


<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
					<a class="brand" href="#">Via!ar</a>
					<div class="nav-collapse">
						<ul class="nav">
							<li class="active">
								<a href="/viajar/Admin/pag_cadastro_pesq.php">Home</a>
							</li>
						
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="#">Viagens <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li>
										<a href="/viajar/Admin/pag_cadastro.php"> Cadastrar</a>
										<a href="/viajar/Admin/pag_cadastro_pesq.php"> Pesquisar</a>
									</li>
								</ul>
							</li>
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="#">Promocoes <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li>
										<a href="/viajar/Admin/promocoes.php"> Cadastrar</a>
										<a href="/viajar/Admin/pag_cadastro_pesq.php"> Pesquisar</a>
									</li>
								</ul>
							</li>
									
								
							
						</ul>
						<ul class="nav pull-right">
							<li>
								<a href="#"><span class="badge badge-warning">Dia</span></a>
							</li>
							<li class="divider-vertical"></li>
							<li>
								<a></a>
							</li>
							<li class="divider-vertical"></li>
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="#">Opcoes <b class="caret"></b></a>
								<ul class="dropdown-menu">
									
									<li class="divider"></li>
									<li>
										<a href="/viajar/pages/controller/logout.php"><i class="icon-off"></i> Logout</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
		<?php 
			if(temParametroGet("resultado") && getParametroGet("resultado")=='sucesso'){
				echo '
					<div class="alert alert-success">
  						<button type="button" class="close" data-dismiss="alert">&times;</button>
  						<strong>Operacao efetuada com sucesso!</strong> .
					</div>
				
				';
			}
			if(temParametroGet("resultado") && getParametroGet("resultado")=='login'){
				echo '
					<div class="alert alert-success">
  						<button type="button" class="close" data-dismiss="alert">&times;</button>
  						<strong>Login efetuado com sucesso!</strong> .
					</div>
				
				';
			}
			if(temParametroGet("resultado") && getParametroGet("resultado")=='error'){
				echo '
					<div class="alert alert-error">
  						<button type="button" class="close" data-dismiss="alert">&times;</button>
  						<strong>Ocorreu um erro!</strong> .
					</div>
				
				';
			}
			if(temParametroGet("error") && getParametroGet("error")=='validacao'){
				echo '
					<div class="alert alert-error">
  						<button type="button" class="close" data-dismiss="alert">&times;</button>
  						<strong>'.getParametroGet("message").'!</strong> .
					</div>
				
				';
			}
		
		?>
		
		</div>