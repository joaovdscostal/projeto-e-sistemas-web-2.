<footer class="footer">
  <div class="container">
  <div class="row">
  <div class="span6"><p><strong>&copy; Via!ar</strong> - Software para companhia de viagem - Sistemas de informação - Redentor</p></div>
  <div class="span6">  <ul class="footer-links">
          <li><a href="#">P&aacute;gina inicial</a></li>
          <li class="muted">·</li>
          <li><a href="#">Sobre</a></li>
           <li class="muted">·</li>
          <li><a href="#">Contato</a></li>
          <li class="muted">·</li>

  <li><div class="btn-group">
  <button rel="tooltip" title="Follow us on Twitter" class="btn btn btn-small"><div class="fs1" aria-hidden="true" data-icon="&#xe0e6;"></div></button>
  <button rel="tooltip" title="Like us on Facebook" class="btn btn btn-small"><div class="fs1" aria-hidden="true" data-icon="&#xe0e3;"></div></button>
  <button rel="tooltip" title="Share us on Google+" class="btn btn-small"><div class="fs1" aria-hidden="true" data-icon="&#xe0df;"></div></button>
</div></li>
</ul>
</div>
</div>

</div>

</footer>


 <!-- End Footer -->



    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>

    <script src="assets/js/bootstrap.js"></script>

    <script src="assets/js/prettyCheckable.js"></script>
    <script src="assets/js/main.js"></script>





  </body>
</html>