<?php 
	include '../pages/top/top.php';
	include '../pages/menu/nav.php';
?>


<div class="container">
<?php include '../pages/menu/slidePesq.php';?>

<div class="row">
<?php include '../pages/menu/menu.php';?>
  

  <div class="span9">


	<div class="viagens" >
		<div class="clear"></div>

			<ul class="nav nav-tabs">
			  <li><a href="cliente.htm">Home</a></li>
			  <li class="active">
				<a href="viagens.htm">Minhas viagens</a>
			  </li>
			  <li><a href="dados.htm">Alterar dados</a></li>
			</ul>

		   <p class="mviagens">Minhas viagens</p>
           <table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Local</th>
						<th>Valor</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>China</td>
						<td>R$ 1.200,00</td>
						<th><a href="#">+ detalhes</a></th>
					</tr>
					<tr>
						<td>2</td>
						<td>Argentina</td>
						<td>R$ 1.100,00</td>
						<th><a href="#">+ detalhes</a></th>
					</tr>
					<tr>
						<td>3</td>
						<td>Padua</td>
						<td>R$ 1,00</td>
						<th><a href="#">+ detalhes</a></th>
					</tr>
					<tr>
						<td>4</td>
						<td>Franca</td>
						<td>R$ 3.442,00</td>
						<th><a href="#">+ detalhes</a></th>
					</tr>
					<tr>
						<td>5</td>
						<td>Iraque</td>
						<td>R$ 1.999,00</td>
						<th><a href="#">+ detalhes</a></th>
					</tr>
					<tr>
						<td>6</td>
						<td>Japao</td>
						<td>R$ 2.999,00</td>
						<th><a href="#">+ detalhes</a></th>
					</tr>
					<tr>
						<td>7</td>
						<td>Bosnia</td>
						<td>R$ 5.999,00</td>
						<th><a href="#">+ detalhes</a></th>
					</tr>
					<tr>
						<td>8</td>
						<td>Africa do Sul</td>
						<td>R$ 7.999,00</td>
						<th><a href="#">+ detalhes</a></th>
					</tr>
					<tr>
						<td>9</td>
						<td>EUA</td>
						<td>R$ 2.999,00</td>
						<th><a href="#">+ detalhes</a></th>
					</tr>
			    </tbody>
		   </table>

        </div><!--viagens -->

</div>
</div>


</div> <!-- End Main Content -->

<?php 
	include '../pages/footer/footerAdmin.php';
?>
