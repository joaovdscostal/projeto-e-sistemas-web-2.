<?php 
	include '../pages/top/top.php';
?>

    <div class="container">

        <div class="login-logo"><h2>via!ar</h2></div>


      <form class="form-signin animated bounce">
        <h2 class="form-signin-heading"><strong>Bem-vindo</strong>, Por favor conecte-se aqui</h2>
        <input type="text" class="input-block-level" placeholder="Endereço de email">
        <input type="password" class="input-block-level" placeholder="Senha">

        <button class="btn btn-large btn-block btn-blue" type="submit">Login</button>
        <hr>


<a href="#" class="pull-right">Esqueceu sua senha?</a>
      </form>

    </div> <!-- /container -->
<?php 
	include '../pages/footer/footerAdmin.php';
?>
