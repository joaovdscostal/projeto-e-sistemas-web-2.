<?php 
	include '../pages/top/top.php';
?>

  <div class="all">
	<h3>via!ar</h3>
  
	<div class="cadastro customBox">
		<p><span>Cadastre-se</span>, e totalmente de graca.</p>
		<form class="form-horizontal">
			<div class="control-group">
				<div class="controls custom">
					<input type="text" placeholder="Usuario">
				</div>
			</div>
			<div class="control-group">
				<div class="controls custom">
					<input type="email" placeholder="Endereco de email">
				</div>
			</div>
			<div class="control-group">
				<div class="controls custom">
					<input type="password" placeholder="Senha">
				</div>
			</div>
			<div class="control-group">
				<div class="controls custom">
					<input type="password" placeholder="Confirmacao de senha">
				</div>
			</div>
			<div class="control-group">
				<div class="controls custom">
					<button type="submit" class="btn btn-medium btn-block btnCadastrar">Cadastrar</button>
				</div>
			</div>
		</form>
	</div>
	
  </div>

   
  <?php 
	include '../pages/footer/footerAdmin.php';
?>
