<?php 
	include '../pages/top/top.php';
	include '../pages/menu/nav.php';
?>


<div class="container">
<?php include '../pages/menu/slidePesq.php';?>

<div class="row">
<?php include '../pages/menu/menu.php';?>
  <div class="span9">


   <div class="viagens" >
		<div class="clear"></div>
			
			<ul class="nav nav-tabs">
			  <li><a href="cliente.htm">Home</a></li>
			  <li><a href="viagens.htm">Minhas viagens</a></li>
			  <li class="active">
				<a href="dados.htm">Alterar dados</a>
			  </li>
			</ul>
			
			
		<div class="clear"></div>
			
			<p>Alterar senha</p>
			
			<div class="alterar">
				<form class="form-horizontal">
					<div class="control-group">
						<div class="controls custom">
							<input type="text" placeholder="Antiga senha">
						</div>
					</div>
					<div class="control-group">
						<div class="controls custom">
							<input type="password" placeholder="Nova senha">
						</div>
					</div>
					<div class="control-group">
						<div class="controls custom">
							<input type="password" placeholder="Confirmacao de nova senha">
						</div>
					</div>
					<div class="control-group">
						<div class="controls custom">
							<button type="submit" class="btn btn-medium btn-block">Alterar</button>
						</div>
					</div>
				</form>
            </div>
			
			<p>Alterar e-mail</p>
			
			<div class="alterar">
				<form class="form-horizontal">
					<div class="control-group">
						<div class="controls custom">
							<input type="email" placeholder="Antigo e-mail">
						</div>
					</div>
					<div class="control-group">
						<div class="controls custom">
							<input type="email" placeholder="Novo e-mail">
						</div>
					</div>
					<div class="control-group">
						<div class="controls custom">
							<input type="email" placeholder="Confirmacao novo e-mail">
						</div>
					</div>
					<div class="control-group">
						<div class="controls custom">
							<button type="submit" class="btn btn-medium btn-block">Alterar</button>
						</div>
					</div>
				</form>
            </div>
		
        </div><!--viagens -->

</div>
</div>


</div> <!-- End Main Content -->

<?php 
	include '../pages/footer/footerAdmin.php';
?>