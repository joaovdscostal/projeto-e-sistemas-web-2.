<?php 
	include '../pages/top/top.php';
	include '../pages/menu/nav.php';
?>

    <div class="container">


 <div class="row">
  
   <div class="span12">
   
<div class="hero-unit animated fadeIn">
  <h1>404</h1>
  <p>This page was not found, it could have been deleted or is no longer active.</p>
  

  <div class="row">
  <div class="span3"><p>

    <a href="index.htm" class="btn btn-large">
     &laquo; Travel back Home
    </a>
  </p></div>
  <div class="span7"><div class="input-append">

  <input class="span5" id="appendedInputButton" placeholder="Search for something..." type="text">
  <button class="btn btn-blue" type="button">Go!</button>
</div></div>
</div>
  
</div>

  </div>
</div>





      


    </div> <!-- /container -->
<?php 
	include '../pages/footer/footerAdmin.php';
?>