<?php 
	include '../pages/top/top.php';
	include '../pages/menu/nav.php';
?>


  <!-- Start Main Content  -->

  <div class="container">

 <?php include '../pages/menu/slidePesq.php';?>

<div class="row">
<?php include '../pages/menu/menu.php';?>

  <div class="span9 prod" style="background-color: #FFFFFF">
	<ul class="breadcrumb">
                   <li><a href="#">Home</a> <span class="divider"></span></li>
                   <li class="active">Viagem</li>
                 </ul>
			<div class="row-fluid detalhes" style="padding-left: 20px;">
						<div class="span5 foto">
							<div class="carousel slide clearfix" id="itemsingle">
								<div class="carousel-inner">
								  <div class="item active">
									<img alt="slider detail dodolan manuk" src="assets/img/h-2.jpg">
								  </div>
								  <div class="item">
									<img alt="slider detail dodolan manuk" src="assets/img/h-2.jpg">
								  </div>
								  <div class="item">
									<img alt="slider detail dodolan manuk" src="assets/img/h-2.jpg">
								  </div>
								</div>

								<ol class="carousel-indicators">
								  <li class="active" data-slide-to="0" data-target="#itemsingle"><img alt="slider detail dodolan manuk" src="assets/img/h-2.jpg"></li>
								  <li class="" data-slide-to="1" data-target="#itemsingle"><img alt="slider detail dodolan manuk" src="assets/img/h-2.jpg"></li>
								  <li class="" data-slide-to="2" data-target="#itemsingle"><img alt="slider detail dodolan manuk" src="assets/img/h-2.jpg"></li>
								</ol>

							</div>
						</div>
						<div class="span7">
							<h3>Disney</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis lectus metus, at posuere neque. Sed pharetra nibh eget orci convallis at posuere leo convallis. Sed blandit augue vitae augue scelerisque bibendum. Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>
							<p>C&oacute;digo : <span class="label label-warning">#2120</span> - Data final : <span class="label label-warning">17 de dezembro de 2015</span></p>
							<h4 class="orderPrice">R$200</h4>
							<p style="text-align: right;"><a class="btn btn-warning btn-large" href="#"><i class="icon-shopping-cart icon-white"></i> Comprar</a></p>
						</div>
					</div>


</div>
</div>


 <!-- End Right Section  -->


</div> <!-- End Main Content -->


 <!-- Start Footer -->

<?php 
	include '../pages/footer/footerAdmin.php';
?>