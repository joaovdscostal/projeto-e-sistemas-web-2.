<?php 
	include '../pages/top/topAdmin.php';
?>
		<div id="login">
			<h3>Login Via!ar</h3>
			<form action="../pages/controller/login.php" method="post">
				<fieldset>
				    <?php if (temParametroGet("error") && getParametroGet("error") == 'validacao') {
					echo '
				    <div class="alert alert-danger">
					    <a class="close" data-dismiss="alert">X</a>
					    '.getParametroGet("message").'
				    </div>
				    ';
				    }?>
					<div class="control-group">
						<label class="control-label" for="user_name">Login</label>
						<div class="controls">
							<input type="text" class="input-large" id="user_name" name="login" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="pass_word">Senha</label>
						<div class="controls">
							<input type="password" class="input-large" id="pass_word" name="senha">
						</div>
					</div>
				</fieldset>
				<a href="forgot_password.html">Esqueceu a Senha ?</a>
				<div class="pull-right">
					<button type="submit" class="btn btn-warning">
						Login
					</button>
				</div>
			</form>
			Put Your Copyright Here &copy; 2012
			<div class="pull-right">
				<a href="register.html">Sign In</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#">Need Help</a>
			</div>
		</div>

<?php 
	include '../pages/footer/footerAdmin.php';
?>