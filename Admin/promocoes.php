﻿<?php 
	include '../pages/top/topAdmin.php';
	include '../pages/menu/menuAdmin.php';
	include '../pages/dao/conexao.php';
    include '../pages/dao/viagem.dao.php';
	
	soAcessaLogadoComoAdministrador();
?>
		
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<div class="page-header-top">
						<div class="container">
							<h1>Via!ar <small>Página de manutenção</small></h1>
						</div>
					</div>
					<div class="page-header">
						<h3>Página para cadastro de promoções</h3>
					</div>
					<div class="row-fluid">
						<div class="form_cad">
                    	<div  class="clearfix"></div>
                       <div class="alinha_form">
                       <form method="post" action="../pages/controller/promocao.php">
                          <fieldset>
                            <legend>Cadastro de promoções</legend>
                            <label>Selecione a viagem:</label>
                            <?php $viagens = buscar_todas_viagens($conexao); ?>
								<?php  
								echo("<select>");
								foreach ($viagens as $viagem){
								echo("<option value='".$viagem['idviagem']."'>".$viagem['nome']."</option>");
								}
								echo("</select>");
								?>
       						</select>
                            <label>Data do final da promoção:</label>
                            <input type="text" placeholder="Digite a data" name="datalimite" id="cad_data">
                            <label>Valor:</label>
                            <input type="text" placeholder="Valor da promoção">
                            <br>
                            <button type="submit" class="btn">Enviar</button>
                          </fieldset>
                        </form>
                        </div>
						</div>
                        </div>								
						</div>
                        
					</div>
				</div>
				
<?php 
	include '../pages/footer/footerAdmin.php';
?>