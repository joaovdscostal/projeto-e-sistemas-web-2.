<?php 
	include '../pages/top/topAdmin.php';
	include '../pages/menu/menuAdmin.php';
	
	soAcessaLogadoComoAdministrador();
?>
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<div class="page-header-top">
						<div class="container">
							<h1>Via!ar <small>Pagina de manutenção</small></h1>
						</div>
					</div>
					<div class="page-header">
						<h3>Pagina para cadastro de viagens </h3>
					</div>
					<div class="row-fluid">
						<div class="span3">
							
									
						</div>
						<div class="form_cad">
                    	<div  class="clearfix"></div>
                       <div class="alinha_form">
                       <form method="post" action="../pages/controller/viagem.php">
                          <fieldset>
                            <legend>Cadastro de viagens </legend>
                            <label>Nome: </label>
                            <input type="text" placeholder="Nome da viagem a ser cadastrada" name="nome">
                            <label>Descricao: </label>
                            <input type="text" placeholder="Descricao" name="descricao">
                            <label>Valor: </label>
                            <input type="text" placeholder="Valor da viagem" name="valor">
                            <label>Data: </label>
                            <input type="text" placeholder="Digite a data " name="datalimite" id="cad_data"><br>
                            <label>Categoria: </label>
                            <input type="text" placeholder="Categoria" name="categoria">
                            <label>Localizacao: </label>
                            <input type="text" placeholder="Localizacao" name="localizacao"></br>
                            <button type="submit" class="btn">Enviar</button>
                          </fieldset>
                        </form>
                        </div>
						</div>
                        </div>								
						</div>
                        
					</div>
				</div>
<?php 
	include '../pages/footer/footerAdmin.php';
?>