<?php 
	include '../pages/top/topAdmin.php';
	include '../pages/menu/menuAdmin.php';
	include '../pages/dao/conexao.php';
	include '../pages/dao/viagem.dao.php';
	
	soAcessaLogadoComoAdministrador();
?>
		

		<div class="container" >
			<div class="row-fluid">
				<div class="span12">
				
				
					<div class="page-header-top">
						<div class="container">
							<h1>Via!ar <small>Pagina de manutencao</small></h1>
						</div>
					</div>
					<div class="page-header">
						<h3>Pagina para pesquisa </h3>
					</div>
					<div class="row-fluid">
						<div class="span3">
							
									
						</div>
							

  
    
    		<div class="fuelux">
                    <div class="datagrid-example">
                        <div style="height:400px;width:900px;margin-bottom:20px;"><table class="table table-bordered datagrid datagrid-stretch-header"><thead>
                                <tr>
                                    <th colspan="4">
                                        <span class="datagrid-header-title">Via!ar</span>
        
                                        <div class="datagrid-header-left">
                                            <div class="input-append search datagrid-search">
                                                <input type="text" class="input-medium" placeholder="Pesquisar">
                                                <button type="button" class="btn"><i class="icon-search"></i></button>
                                            </div>
                                        </div>
                                        <div class="datagrid-header-right">
                                            <div class="select filter" data-resize="auto">
                                                <button type="button" data-toggle="dropdown" class="btn dropdown-toggle">
                                                    <span class="dropdown-label" style="width: 110px;">Todas</span>
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li data-value="all"><a href="#">Todas</a></li>
                                                    <li data-value="lt5m"><a href="#">Nacionais</a></li>
                                                    <li data-value="gte5m"><a href="#">Internacionais</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                                <tr>
                                	<th data-property="toponymName" class="sortable" style="width: 80px;">Codigo</th>
                                	<th data-property="countrycode" class="sortable" style="width: 100px;">Nome</th>
                                	<th data-property="population" class="sortable" style="width: 220px;">Descricao</th>
                                	<th data-property="fcodeName" class="sortable" style="width: 70px;">Valor</th>
                                	<th data-property="fcodeName" class="sortable" style="width: 80px;">Data limite</th>
                                	<th data-property="fcodeName" class="sortable" style="width: 130px;">Categoria</th>
                                	<th data-property="fcodeName" class="sortable">Localizacao</th>
                                </tr>
                                </thead>
                              </table>
                            <div class="datagrid-stretch-wrapper" style="height: 256px;">
                            
                            <table id="MyGrid" class="table table-bordered datagrid">
        						<tbody>
        						
        						<?php 
        							$viagens = buscar_todas_viagens($conexao);
        							if (count($viagens) == 0){
        								echo '
        									<tr class="error">
        										<td colspan="7">Viagens nao cadastradas</td>
        									</tr>
        								';
        							}else{
        								foreach ($viagens as $viagem){
        									echo '
        										<tr>
        											<td style="width: 80px;">'.$viagem["idviagem"].'</td>
        											<td style="width: 100px;">'.$viagem["nome"].'</td>
        											<td style="width: 220px;">'.$viagem["descricao"].'</td>
        											<td style="width: 70px;">R$'.$viagem["valor"].'</td>
        											<td style="width: 80px;">'.$viagem["datalimite"].'</td>
        											<td style="width: 130px;">'.$viagem["categoria"].'</td>
        											<td>'.$viagem["localizacao"].'</td>
        										</tr>
        									';
        								}
        							}
        						?>
        							
        						</tbody>
        
                                
        
                            </table></div>
                        <table class="table table-bordered datagrid datagrid-stretch-footer"><tfoot>
                                <tr>
                                    <th colspan="4">
                                        <div class="datagrid-footer-left" style="visibility: visible;">
                                        </div>
                                        <div class="datagrid-footer-right" style="visibility: visible;">
                                            <div class="grid-pager">
                                                <button type="button" class="btn grid-prevpage" ><i class="icon-chevron-left"></i></button>
                                                <button type="button" class="btn grid-nextpage"><i class="icon-chevron-right"></i></button>
                                           		<br/>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                                </tfoot></table><br/></div>
                    </div>
        
                    <input type="button" class="btn" id="datagrid-reload" value="Recarregar">	    
		

</div>
						</div>
                        </div>								
						</div>
                        
					</div>
				</div>
			</div>

			<hr>
			<footer>

			</footer>

		</div>
	
<?php 
	include '../pages/footer/footerAdmin.php';
?>