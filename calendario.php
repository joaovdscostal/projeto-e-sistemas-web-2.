<h3><?php echo "Teste de integracao php" ?></h3>

<?php 
	function linha($semana){
		
		
		echo "<tr>";
		
		for ($i = 0; $i < 7; $i++) {
			if(isset($semana[$i])){
				echo "<td>{$semana[$i]}</td>";
			}else{
				echo "<td></td>";
			}
		}

			
		echo "</tr>";
	}
	
	function calendario(){
		
		$dia = 1;
		$semana = array();
		
		while ($dia <= 31) {
			array_push($semana, $dia);
			
			if(count($semana) == 7){
				linha($semana);
				$semana = array();
			}
			
			$dia++;
		}
		linha($semana);
	}

?>

<table border="1px">
	<tr>
		<th>Seg</th>
		<th>Ter</th>
		<th>Qua</th>
		<th>Qui</th>
		<th>Sex</th>
		<th>Sab</th>
		<th>Dom</th>
	</tr>
	<?php calendario()?>

</table>